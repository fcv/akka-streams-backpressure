# Akka Streams Backpressure demo

Simple demo code based on [Rock the JVM](https://www.youtube.com/channel/UCRS4DvO9X7qaqVYUW2_dwOw)'s
[Akka Streams Backpressure - a Demonstration](https://www.youtube.com/watch?v=L5FAyCCWGL0) YouTube video.


## Output example

```
$ sbt run
[info] welcome to sbt 1.5.5 (Ubuntu Java 11.0.11)
[info] loading global plugins from /home/fcv/.sbt/1.0/plugins
[info] loading settings for project akka-streams-backpressure-build from plugins.sbt ...
[info] loading project definition from /home/fcv/projects/akka-streams-backpressure/project
[info] loading settings for project akka-streams-backpressure from build.sbt ...
[info] set current project to akka-streams-backpressure (in build file:/home/fcv/projects/akka-streams-backpressure/)
[info] running br.fcv.AkkaStreamsBackpressure 
[2021-11-08 10:37:46,188] [INFO] [akka.event.slf4j.Slf4jLogger] [StreamsSystem-akka.actor.default-dispatcher-3] [] - Slf4jLogger started
[2021-11-08 10:37:46,329] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 1
[2021-11-08 10:37:46,330] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 2
[2021-11-08 10:37:46,330] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 1 to 10
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 3
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 2 to 20
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 4
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 3 to 30
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 5
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 4 to 40
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 6
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 5 to 50
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 7
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 6 to 60
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 8
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 7 to 70
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 9
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 8 to 80
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 10
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 9 to 90
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 11
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 10 to 100
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 12
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 11 to 110
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 13
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 12 to 120
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 14
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 13 to 130
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 15
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 14 to 140
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 16
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 15 to 150
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 17
[2021-11-08 10:37:46,331] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 16 to 160
[2021-11-08 10:37:46,431] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 10
[2021-11-08 10:37:46,531] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 20
[2021-11-08 10:37:46,632] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 30
[2021-11-08 10:37:46,732] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 40
[2021-11-08 10:37:46,833] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 50
[2021-11-08 10:37:46,933] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 60
[2021-11-08 10:37:47,034] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 70
[2021-11-08 10:37:47,035] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 18
[2021-11-08 10:37:47,035] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 17 to 170
[2021-11-08 10:37:47,035] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 19
[2021-11-08 10:37:47,036] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 18 to 180
[2021-11-08 10:37:47,036] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 20
[2021-11-08 10:37:47,036] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 19 to 190
[2021-11-08 10:37:47,036] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 21
[2021-11-08 10:37:47,036] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 20 to 200
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 22
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 21 to 210
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 23
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 22 to 220
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 24
[2021-11-08 10:37:47,037] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 23 to 230
[2021-11-08 10:37:47,038] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 25
[2021-11-08 10:37:47,038] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 24 to 240
[2021-11-08 10:37:47,134] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 80
[2021-11-08 10:37:47,235] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 90
[2021-11-08 10:37:47,335] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 100
[2021-11-08 10:37:47,436] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 110
[2021-11-08 10:37:47,536] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 120
[2021-11-08 10:37:47,637] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 130
[2021-11-08 10:37:47,737] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 140
[2021-11-08 10:37:47,838] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 150
[2021-11-08 10:37:47,838] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 26
[2021-11-08 10:37:47,838] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 25 to 250
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 27
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 26 to 260
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 28
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 27 to 270
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 29
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 28 to 280
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 30
[2021-11-08 10:37:47,839] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 29 to 290
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 31
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 30 to 300
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 32
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 31 to 310
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 33
[2021-11-08 10:37:47,840] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 32 to 320
[2021-11-08 10:37:47,938] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 160
[2021-11-08 10:37:48,039] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 170
[2021-11-08 10:37:48,139] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 180
[2021-11-08 10:37:48,239] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 190
[2021-11-08 10:37:48,340] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 200
[2021-11-08 10:37:48,440] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 210
[2021-11-08 10:37:48,541] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 220
[2021-11-08 10:37:48,641] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 230
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 34
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 33 to 330
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 35
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 34 to 340
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 36
[2021-11-08 10:37:48,642] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 35 to 350
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 37
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 36 to 360
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 38
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 37 to 370
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 39
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 38 to 380
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 40
[2021-11-08 10:37:48,643] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 39 to 390
[2021-11-08 10:37:48,644] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 41
[2021-11-08 10:37:48,644] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 40 to 400
[2021-11-08 10:37:48,742] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 240
[2021-11-08 10:37:48,842] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 250
[2021-11-08 10:37:48,943] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 260
[2021-11-08 10:37:49,043] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 270
[2021-11-08 10:37:49,143] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 280
[2021-11-08 10:37:49,244] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 290
[2021-11-08 10:37:49,344] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 300
[2021-11-08 10:37:49,445] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 310
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 42
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 41 to 410
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 43
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 42 to 420
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 44
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 43 to 430
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 45
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 44 to 440
[2021-11-08 10:37:49,446] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 46
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 45 to 450
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 47
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 46 to 460
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 48
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 47 to 470
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 49
[2021-11-08 10:37:49,447] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 48 to 480
[2021-11-08 10:37:49,545] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 320
[2021-11-08 10:37:49,646] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 330
[2021-11-08 10:37:49,746] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 340
[2021-11-08 10:37:49,847] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 350
[2021-11-08 10:37:49,947] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 360
[2021-11-08 10:37:50,048] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 370
[2021-11-08 10:37:50,148] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 380
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 390
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 50
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 49 to 490
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 51
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 50 to 500
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 52
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 51 to 510
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 53
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 52 to 520
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 54
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 53 to 530
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 55
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 54 to 540
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 56
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 55 to 550
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 57
[2021-11-08 10:37:50,249] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 56 to 560
[2021-11-08 10:37:50,349] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 400
[2021-11-08 10:37:50,449] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 410
[2021-11-08 10:37:50,550] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 420
[2021-11-08 10:37:50,650] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 430
[2021-11-08 10:37:50,751] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 440
[2021-11-08 10:37:50,851] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 450
[2021-11-08 10:37:50,951] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 460
[2021-11-08 10:37:51,052] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 470
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 58
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 57 to 570
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 59
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 58 to 580
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 60
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 59 to 590
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 61
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 60 to 600
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 62
[2021-11-08 10:37:51,053] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 61 to 610
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 63
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 62 to 620
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 64
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 63 to 630
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 65
[2021-11-08 10:37:51,054] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 64 to 640
[2021-11-08 10:37:51,152] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 480
[2021-11-08 10:37:51,253] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 490
[2021-11-08 10:37:51,353] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 500
[2021-11-08 10:37:51,454] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 510
[2021-11-08 10:37:51,554] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 520
[2021-11-08 10:37:51,655] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 530
[2021-11-08 10:37:51,755] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 540
[2021-11-08 10:37:51,855] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 550
[2021-11-08 10:37:51,856] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 66
[2021-11-08 10:37:51,856] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 65 to 650
[2021-11-08 10:37:51,856] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 67
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 66 to 660
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 68
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 67 to 670
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 69
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 68 to 680
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 70
[2021-11-08 10:37:51,857] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 69 to 690
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 71
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 70 to 700
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 72
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 71 to 710
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 73
[2021-11-08 10:37:51,858] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 72 to 720
[2021-11-08 10:37:51,956] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 560
[2021-11-08 10:37:52,056] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 570
[2021-11-08 10:37:52,157] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 580
[2021-11-08 10:37:52,257] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 590
[2021-11-08 10:37:52,358] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 600
[2021-11-08 10:37:52,458] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 610
[2021-11-08 10:37:52,559] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 620
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 630
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 74
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 73 to 730
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 75
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 74 to 740
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 76
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 75 to 750
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 77
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 76 to 760
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 78
[2021-11-08 10:37:52,659] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 77 to 770
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 79
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 78 to 780
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 80
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 79 to 790
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 81
[2021-11-08 10:37:52,660] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 80 to 800
[2021-11-08 10:37:52,759] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 640
[2021-11-08 10:37:52,860] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 650
[2021-11-08 10:37:52,960] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 660
[2021-11-08 10:37:53,061] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 670
[2021-11-08 10:37:53,161] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 680
[2021-11-08 10:37:53,261] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 690
[2021-11-08 10:37:53,362] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 700
[2021-11-08 10:37:53,462] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 710
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 82
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 81 to 810
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 83
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 82 to 820
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 84
[2021-11-08 10:37:53,463] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 83 to 830
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 85
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 84 to 840
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 86
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 85 to 850
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 87
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 86 to 860
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 88
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 87 to 870
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 89
[2021-11-08 10:37:53,464] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 88 to 880
[2021-11-08 10:37:53,563] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 720
[2021-11-08 10:37:53,663] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 730
[2021-11-08 10:37:53,764] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 740
[2021-11-08 10:37:53,864] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 750
[2021-11-08 10:37:53,964] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 760
[2021-11-08 10:37:54,065] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 770
[2021-11-08 10:37:54,165] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 780
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 790
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 90
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 89 to 890
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 91
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 90 to 900
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 92
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 91 to 910
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 93
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 92 to 920
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 94
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 93 to 930
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 95
[2021-11-08 10:37:54,266] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 94 to 940
[2021-11-08 10:37:54,267] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 96
[2021-11-08 10:37:54,267] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 95 to 950
[2021-11-08 10:37:54,267] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 97
[2021-11-08 10:37:54,267] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 96 to 960
[2021-11-08 10:37:54,366] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 800
[2021-11-08 10:37:54,467] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 810
[2021-11-08 10:37:54,567] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 820
[2021-11-08 10:37:54,668] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 830
[2021-11-08 10:37:54,768] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 840
[2021-11-08 10:37:54,868] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 850
[2021-11-08 10:37:54,969] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 860
[2021-11-08 10:37:55,069] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 870
[2021-11-08 10:37:55,070] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 98
[2021-11-08 10:37:55,070] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 97 to 970
[2021-11-08 10:37:55,070] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Producing 99
[2021-11-08 10:37:55,070] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 98 to 980
[2021-11-08 10:37:55,072] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-7] [] - Mapping 99 to 990
[2021-11-08 10:37:55,170] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 880
[2021-11-08 10:37:55,270] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 890
[2021-11-08 10:37:55,371] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 900
[2021-11-08 10:37:55,471] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 910
[2021-11-08 10:37:55,572] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 920
[2021-11-08 10:37:55,672] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 930
[2021-11-08 10:37:55,772] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 940
[2021-11-08 10:37:55,873] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 950
[2021-11-08 10:37:55,979] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 960
[2021-11-08 10:37:56,080] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 970
[2021-11-08 10:37:56,180] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 980
[2021-11-08 10:37:56,280] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [StreamsSystem-akka.actor.default-dispatcher-6] [] - Consuming 990
[2021-11-08 10:37:56,284] [DEBUG] [br.fcv.AkkaStreamsBackpressure$] [scala-execution-context-global-128] [] - Done
[success] Total time: 11 s, completed Nov 8, 2021, 10:37:56 AM
```
