package br.fcv

import akka.Done
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.{ExecutionContext, Future}

// Based on https://www.youtube.com/watch?v=L5FAyCCWGL0
object AkkaStreamsBackpressure extends StrictLogging {

  private implicit val system: ActorSystem[_] = ActorSystem(Behaviors.empty, "StreamsSystem")

  private val source = Source {
    LazyList.range(1, 100)
      .map { i =>
        logger.debug(s"Producing $i")
        i
      }
  }

  private val flow = Flow[Int].map { i =>
    val v = i * 10
    logger.debug(s"Mapping $i to $v")
    v
  }

  private val sink = Sink.foreach[Int] { i =>
    Thread.sleep(100)
    logger.debug(s"Consuming $i")
  }

  private val graph: RunnableGraph[Future[Done]] = source.via(flow).async.toMat(sink)(Keep.right)

  def main(args: Array[String]): Unit = {
    val f = graph.run()
    f.onComplete { _ =>
      logger.debug("Done")
      system.terminate()
    }(ExecutionContext.global)
  }
}
